package TC1;

import static org.testng.Assert.assertEquals;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Calculator 
{
	@Test
	public static void clickEquals()
		{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\twitk\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://web2.0calc.com/");
		driver.findElement(By.xpath(".//*[@id=\"cookieconsentform\"]/div/div/div[2]/div[1]/button")).click();
		driver.findElement(By.xpath(".//*[@id=\"input\"]")).sendKeys("35*999+(100/4)");
		driver.findElement(By.xpath(".//*[@id=\"BtnCalc\"]")).click();
		driver.findElement(By.xpath(".//*[@id=\"hist\"]/button[2]")).click();
	    driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		assertEquals(driver.findElement(By.xpath(".//*[@id=\"histframe\"]/ul/li/p[1]")).getAttribute("title"), "34990");
		driver.get("https://web2.0calc.com/");
		driver.findElement(By.xpath(".//*[@id=\"trigorad\"]")).click();
		driver.findElement(By.xpath(".//*[@id=\"BtnCos\"]")).click();
		driver.findElement(By.xpath(".//*[@id=\"BtnPi\"]")).click();
		driver.findElement(By.xpath(".//*[@id=\"BtnParanR\"]")).click();		
		driver.findElement(By.xpath(".//*[@id=\"BtnCalc\"]")).click();
		driver.findElement(By.xpath(".//*[@id=\"hist\"]/button[2]")).click();
	    driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		assertEquals(driver.findElement(By.xpath(".//*[@id=\"histframe\"]/ul/li[1]/p[1]")).getAttribute("title"), "-1");
		driver.get("https://web2.0calc.com/");
		driver.findElement(By.xpath(".//*[@id=\"BtnSqrt\"]")).click();
		driver.findElement(By.xpath(".//*[@id=\"input\"]")).sendKeys("81)");
		driver.findElement(By.xpath(".//*[@id=\"BtnCalc\"]")).click();
		driver.findElement(By.xpath(".//*[@id=\"hist\"]/button[2]")).click();
	    driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		assertEquals(driver.findElement(By.xpath(".//*[@id=\"histframe\"]/ul/li[1]/p[1]")).getAttribute("title"), "9");
		assertEquals(driver.findElement(By.cssSelector("#histframe > ul > li:nth-child(1) > p.l")).getAttribute("title"), "sqrt(81)");
		assertEquals(driver.findElement(By.cssSelector("#histframe > ul > li:nth-child(2) > p.l")).getAttribute("title"), "cos(pi)");
		assertEquals(driver.findElement(By.cssSelector("#histframe > ul > li:nth-child(3) > p.l")).getAttribute("title"), "35*999+(100/4)");		
		driver.close();
		}
}